# Introduction
Windows programs which don’t have a dedicated version for Mac OS X cannot be ran on Mac directly, but they can be ran with the help of a special tool. There are two basic ways to run Windows programs on a Mac. One is emulation, and another is virtualization.

Emulation refers to simulating the basic portions of the Windows environment necessary to run some Windows programs on a non-Windows system. The most well known emulator is WINE, which is a humorous acronym for “WINE Is Not an Emulator”. While it is possible to build WINE on Mac an official pre-built binary package is not available, and building it yourself is probably exceedingly difficult. That’s where third party WINE distributions come in.

Likely the best among them is **PlayOnMac** which comes as a standard Mac app and features an easy to use interface with which you can browse and install compatible Windows applications.

# My story in publish script to PLAYONMAC
I have been written a script last month. I have put two methods on the script, mainly LOCAL (select existing file) and DOWNLOAD (from host website and auto-setup). However, I met the issue of project policy on PLAYONMAC that WarpPLS is commercial software, not freeware. Therefore, I deleted the method "DOWNLOAD". 

# CHANGELOG
*  Add function "Auto check and download" WINE 3.0.3 on PLAYONMAC
*  Removed method "DOWNLOAD"
*  Only support install version 7

# KNOWN ISSUE
*  Doesn't work WINE 4.0.3 -> 5.0

## Rquirement installation before setup WarpPLS on Mac
Download:
> [XQuartz](https://dl.bintray.com/xquartz/downloads/XQuartz-2.7.11.dmg)

> [Wine Mono](https://download.mono-project.com/archive/6.8.0/macos-10-universal/MonoFramework-MDK-6.8.0.96.macos10.xamarin.universal.pkg)

> PLAYONMAC

## What's PLAYONMAC? Why use it? 
PLAYONMAC is a graphical front-end for Wine compatibility layer. It simplifies the installation of Windows apps and (especiall)y games on GNU/Linux by auto-configuring Wine. It provides wrapper shell scripts to specify the configuration of Wine for any particular software. It also uses an online database of scripts to apply for different programs, and a manual installation can be performed if the script is not available.

Download:
> [Playonmac](http://repository.playonmac.com/PlayOnMac/PlayOnMac_4.3.4.dmg)

Incompatibility:
> `Does not work with macOS Catalina (v10.15) and next, because it drop 32bits support`

# Guideline
## Step 1: Click choose Install button / Install a program

<img src="installRe.png" alt="Install WarpPLS on playonmac"/>

<img src="search_warppls.png" alt="search WarpPLS on playonmac"/>

<img src="intro_warppls.png" alt="Introduction WarpPLS on playonmac"/>

<img src="download_wine_warppls.png" alt="download wine on playonmac"/>

## Step 2: Select existing file 

<img src="selected_warppls.png" alt="select existing file"/>

<img src="install_warppls.png" alt="install warppls on playonmac"/>

## Step 3: Run shortcut on desktop / on Playonmac

<img src="launch_warppls.png" alt="completed installation"/>

<img src="run_warppls.png" alt="run warppls on playonmac"/>
